package no.hig.rosland.daniel.exercise_1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {
    protected String name;
    DBOpenHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dbHelper = new DBOpenHelper(this);
        final Cursor c = dbHelper.getName();
        if (c.moveToFirst() && !c.getString(c.getColumnIndex("name")).equals("")) {
            name = c.getString(c.getColumnIndex("name"));

            TextView textView = (TextView) findViewById(R.id.textView);
            textView.setText("We've saved your name, " + name + "\nPlease press the button marked Second");
        }
    }

    public void showMap(View v) {
        Intent i = new Intent(this, MapsActivity.class);
        i.putExtra("name", name);
        startActivity(i);
    }

    public void readName(View v) {
        EditText editName = (EditText) findViewById(R.id.editName);
        name = editName.getText().toString();

        dbHelper.updateName(name);

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("To further use this app, " + name + "\nPlease press the button marked Second");

    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("name", name);
        super.onSaveInstanceState(savedInstanceState);
    }

    public class DBOpenHelper extends SQLiteOpenHelper {
        public static final String DATABASE_NAME = "NameDB.db";
        private static final int DATABASE_VERSION = 1;
        private static final String COLUMN_ID = "_id";
        private static final String COLUMN_NAME = "name";
        private static final String TABLE_NAME = "names";

        public DBOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /**
         * onCreate makes a "dummy" value in the database as a name.
         * This is further utilized in MainActivity, as it checks if the name is an empty string.
         */

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_NAME + " TEXT)");
            ContentValues cV = new ContentValues();
            cV.put(COLUMN_ID, 1);
            cV.put(COLUMN_NAME, "");
            db.insert(TABLE_NAME, null, cV);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

        /**
         * Takes name for parameter, returns true if it did manage to update name
         * @param name
         * @return
         */

        public boolean updateName(String name) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cV = new ContentValues();
            cV.put(COLUMN_NAME, name);
            db.update(TABLE_NAME, cV, COLUMN_ID + " =?", new String[] { Integer.toString(1) });
            return true;
        }

        public Cursor getName() {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor name = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " +
                COLUMN_ID + " =?", new String[] { Integer.toString(1) });
            return name;
        }
    }
}
